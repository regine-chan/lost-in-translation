import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import store from '../store/store';
import { translate } from '../services/translation.service';
import { addTranslation } from '../services/translation.service';
import { useDispatch } from 'react-redux';
import { addTranslationAction } from '../store/actions/translation.actions';
import "./translate.view.css";

const TranslateView = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    const [input, setInput] = useState("");

    const [validChars, setValidChars] = useState([]);

    useEffect(() => {
        if (store.getState().user === null) {
            history.push("/login");
        }
    });

    const handleTranslateChanged = event => setInput(event.target.value);

    const handleTranslate = event => {
        let temp = translate(input);
        setValidChars(temp);
        addTranslation(temp.join(""));
        dispatch(addTranslationAction(temp.join("")));
    };


    return (
        <div>
            <div className="translateContainer">
                <input className="textInput" type="text" name="translate" id="translate" placeholder="Enter a phrase to translate..." onChange={handleTranslateChanged} />
                <button type="button" onClick={handleTranslate} disabled={input.length === 0}>Translate</button>
            </div>
            <div className="translationResultContainer">
                <ul className="flexContainer">
                    {validChars.map((char, index) => {
                        let imageSrc = "/LostInTranslation_Resources/individial_signs/" + char + ".png";
                        if (char !== " ") {
                            return (
                                <div key={index}>
                                    <img src={imageSrc} alt={char} />
                                </div>
                            );
                        } else {
                            return (
                                <div key={index}>
                                    <br />
                                </div>
                            );
                        }

                    })
                    }
                </ul>
            </div>
        </div>
    );
}

export default TranslateView;