import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import './login.view.css';

import { setUser } from '../services/session.service';
import { setUserAction } from '../store/actions/user.actions';
import store from '../store/store';

const LoginView = () => {

    const dispatch = useDispatch();

    const [username, setUsername] = useState("");

    const history = useHistory();

    const handleLogin = (event) => {
        if (username !== "") {
            dispatch(setUserAction(username));
            setUser(username);
            history.push("/translate");
        }
    }

    useEffect(() => {
        if (store.getState().user !== null) {
            history.push("/translate");
        }
    })


    const handleUsernameChange = event => setUsername(event.target.value);

    return (
        <div className="container">
            <img src="/LostInTranslation_Resources/Logo-Hello.png" alt="" />
            <div className="usernameContainer">
                <input className="textInput" type="text" id="username" name="username" placeholder="Enter your username..." onChange={handleUsernameChange} />
                <button type="button" onClick={handleLogin} disabled={username.length === 0}>Login</button>
            </div>

        </div>
    );
}

export default LoginView;