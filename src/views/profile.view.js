import React from 'react';
import { useState } from 'react';
import store from '../store/store';

const ProfileView = () => {

    const [translations, setTranslations] = useState(store.getState().translations);

    return (
        <div>
            Hello from profile view
            <div>
                <ul>
                    {translations.map(translation => {
                        return (
                            <div>
                                {translation}
                            </div>
                        );
                    })}
                </ul>

            </div>
        </div>
    );
}

export default ProfileView;