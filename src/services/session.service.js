export const setUser = (username) => {
    if (!exists()) {
        localStorage.setItem("username", username);
    }
}

export const exists = () => {
    const user = localStorage.getItem("username");
    if (user !== null) {
        return true;
    }
    return false;
}

export const getUser = () => {
    if (exists()) {
        return localStorage.getItem("username");
    }
    return false;
}

export const removeUser = () => {
    localStorage.clear();
}

