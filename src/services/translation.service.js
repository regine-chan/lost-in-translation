
export const translate = translationText => {
    return translationText.split("")
        .filter(char => (validChars.includes(char.toLowerCase())));
}

export const addTranslation = translation => {
    let translations = localStorage.getItem("translations");
    if (translations !== null) {
        if (translations.length < 10) {
            localStorage.setItem("translations", JSON.stringify([...translations, translation]));
        } else {
            localStorage.setItem("translations", JSON.stringify([...translations.slice(1, 9), translation]));
        }
    } else {
        localStorage.setItem("translations", JSON.stringify([translations]));
    }
}

export const getTranslations = () => {
    let translations = localStorage.getItem("translations");
    if (translations !== null) {
        return JSON.parse(translations);
    }
    return null;
}


const validChars = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", " "];