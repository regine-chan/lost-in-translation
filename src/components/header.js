import React from 'react';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import store from '../store/store';
import './header.css';

import { removeUser } from '../services/session.service';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { removeUserAction } from '../store/actions/user.actions';
import { removeTranslationsAction } from '../store/actions/translation.actions';

const Header = () => {

    const dispatch = useDispatch();

    let [user, setUser] = useState(store.getState().user ? store.getState().user : null);

    useEffect(() => {
        store.subscribe(() => {
            setUser(store.getState().user);
        });
    })

    const logout = () => {
        removeUser();
        dispatch(removeUserAction());
        dispatch(removeTranslationsAction());
    }

    const getUserElement = () => {
        if (user !== null) {
            return (
                <>
                    <li className="right"><Link className="headerElement" to="/login" onClick={logout}>LOGOUT</Link></li>
                    <li className="right"><Link className="headerElement" to="/profile">{user}</Link></li>

                </>
            );
        }
    }

    return (
        <nav className="navHeader">
            <ul>
                <li className="left"><Link className="headerElement left" to="/translate">TRANSLATOR</Link></li>
                {getUserElement()}
            </ul>

        </nav>
    );
}

export default Header;