import React from 'react';
import './App.css';
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom';

import LoginView from './views/login.view';
import TranslateView from './views/translate.view';
import Header from './components/header';
import ProfileView from './views/profile.view';

function App() {

  return (
    <>
      <Header />
      <Switch>
        <Route path="/login">
          <LoginView />
        </Route>
        <Route path="/translate">
          <TranslateView />
        </Route>
        <Route path="/profile">
          <ProfileView />
        </Route>
        <Route path="/" >
          <Redirect to="/login"></Redirect>
        </Route>
      </Switch>
    </>
  );
}

export default App;
