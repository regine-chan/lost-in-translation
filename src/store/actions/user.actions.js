export const ACTION_SET_USER = "ACTION_SET_USER";
export const ACTION_REMOVE_USER = "ACTION_REMOVE_USER";

export const setUserAction = (user = null) => ({
    type: ACTION_SET_USER,
    payload: user
});

export const removeUserAction = () => ({
    type: ACTION_REMOVE_USER
});