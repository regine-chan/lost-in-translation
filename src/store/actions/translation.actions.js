export const ACTION_ADD_TRANSLATION = "ACTION_ADD_TRANSLATION";
export const ACTION_REMOVE_TRANSLATIONS = "ACTION_REMOVE_TRANSLATIONS";

export const addTranslationAction = (translation = "") => ({
    type: ACTION_ADD_TRANSLATION,
    payload: translation
});

export const removeTranslationsAction = () => ({
    type: ACTION_REMOVE_TRANSLATIONS
});