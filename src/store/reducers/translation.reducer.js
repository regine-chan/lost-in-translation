import { ACTION_ADD_TRANSLATION, ACTION_REMOVE_TRANSLATIONS } from '../actions/translation.actions';

export const translationReducer = (state = [], action) => {

    switch (action.type) {
        case ACTION_ADD_TRANSLATION:
            if (state !== null) {
                return state.length < 10 ? [...state, action.payload] : [...state.slice(1, 9), action.payload];
            }
        case ACTION_REMOVE_TRANSLATIONS:
            return [];
        default:
            return state;
    }
}