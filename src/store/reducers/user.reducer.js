import { ACTION_SET_USER, ACTION_REMOVE_USER } from '../actions/user.actions';

export const userReducer = (state = null, action) => {

    switch (action.type) {
        case ACTION_SET_USER:
            return action.payload;
        case ACTION_REMOVE_USER:
            return null;
        default:
            return state;
    }
}