import { createStore, combineReducers } from 'redux';
import { userReducer } from './reducers/user.reducer';
import { getUser } from '../services/session.service';
import { translationReducer } from './reducers/translation.reducer';
import { getTranslations } from '../services/translation.service';

const initialUser = getUser() || null;
const initialTranslations = getTranslations() || null;

const rootReducers = combineReducers({
    user: userReducer,
    translations: translationReducer
});
const store = createStore(rootReducers, { user: initialUser, translations: initialTranslations }, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;